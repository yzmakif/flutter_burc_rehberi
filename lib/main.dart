import 'package:flutter/material.dart';
import 'package:flutter_burc_rehberi/ui/burc_detay.dart';

import 'ui/burc_liste.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Burc Rehberi",
      theme: ThemeData(
        primaryColor: Colors.pink,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        BurcListe.routeName: (context) => BurcListe(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == BurcDetay.routeName) {
          return MaterialPageRoute(
            builder: (context) => BurcDetay(settings.arguments),
          );
        }
      },
    );
  }
}
