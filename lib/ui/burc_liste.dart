import 'package:flutter/material.dart';
import 'package:flutter_burc_rehberi/modal/burc.dart';
import 'package:flutter_burc_rehberi/utils/strings.dart';

class BurcListe extends StatelessWidget {
  static const routeName = "/";
  List<Burc> burclar;
  @override
  Widget build(BuildContext context) {
    burclar = veriKaynaginiHazirla();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          BurcData.APP_NAME,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: listeyiHazirla(context),
    );
  }

  List<Burc> veriKaynaginiHazirla() {
    List<Burc> liste = List.generate(BurcData.BURC_ADLARI.length, (index) {
      return Burc(
        BurcData.BURC_ADLARI[index],
        BurcData.BURC_TARIHLERI[index],
        BurcData.BURC_GENEL_OZELLIKLERI[index],
        "images/${BurcData.BURC_ADLARI[index].toLowerCase()}${index + 1}.png",
        "images/${BurcData.BURC_ADLARI[index].toLowerCase()}_buyuk${index + 1}.png",
      );
    });
    return liste;
  }

  Widget listeyiHazirla(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return listeElemani(context, index);
      },
      itemCount: burclar.length,
    );
  }

  Widget listeElemani(BuildContext context, int index) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.all(10),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: ListTile(
          onTap: () => Navigator.pushNamed(context, "/burcDetay",
              arguments: burclar[index]),
          leading: Image.asset(
            burclar[index].burcKucukResim,
            width: 64,
            height: 64,
          ),
          title: Text(
            burclar[index].burcAdi,
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w400,
                color: Colors.pink.shade500),
          ),
          subtitle: Padding(
            padding: EdgeInsets.symmetric(vertical: 4),
            child: Text(
              burclar[index].burcTarihi,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: Colors.black38),
            ),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            color: Colors.pink,
          ),
        ),
      ),
    );
  }
}
