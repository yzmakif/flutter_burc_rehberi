import 'package:flutter/material.dart';
import 'package:flutter_burc_rehberi/modal/burc.dart';
import 'package:palette_generator/palette_generator.dart';

class BurcDetay extends StatefulWidget {
  static const routeName = "/burcDetay";
  Burc burc;
  BurcDetay(this.burc);
  @override
  _BurcDetayState createState() => _BurcDetayState();
}

class _BurcDetayState extends State<BurcDetay> {
  Color color = Colors.pink;
  Future<PaletteGenerator> palette;
  @override
  void initState() { 
    super.initState();
    palette = PaletteGenerator.fromImageProvider(
      AssetImage(widget.burc.burcBuyukResim),
    ).then((value) {
      setState(() {
        color = value.vibrantColor.color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        primary: true,
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            backgroundColor: color,
            expandedHeight: 250,
            flexibleSpace: FlexibleSpaceBar(
              title: Text("${widget.burc.burcAdi} Burcu"),
              background: Image.asset(
                widget.burc.burcBuyukResim,
                fit: BoxFit.cover,
              ),
              centerTitle: true,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.pink.shade50),
              child: SingleChildScrollView(
                child: Text(
                  widget.burc.burcDetay,
                  style: TextStyle(fontSize: 17, color: Colors.black),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
